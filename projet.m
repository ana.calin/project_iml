%% PROJET IML 

% Nous avons decide d'utiliser pour notre projet les donnees issues du domaine bancaire pour appliquer plusieurs types d'algorithmes de classification binaire vus en cours.

%% 1. Preparation des donnees
%% 1.2 Suppression des NAN et One-Hot Encoding
%Pour la premiere partie du projet nous nous sommes concentres sur l'encodage sous forme de variables 0-1 car la plupart des variables du dataset ne le sont pas sous ce format. 
% Nous avons aussi traite le probleme des variables contenant le champ :
% 'Unknown' et NAN que nous avons supprime.
% Pour les variables contenant plus d'une valeur (ex : Day <=> 7 jours )
% nous avons cree un vecteur pour chaque valeur avec des booleans : 1 si x_i correspond a cette valeur, 0 sinon. 
% Nous allons recuperer alors une matrice X contenant tous les variables discretisees. 
[X,Y] = preparation_donnees();

%% 2. Apercu des donnees
%Avant de nous lancer dans la classification des données, nous avons essayé de voir s'il existe des relations entre nos variables. Pour cela nous avons realié la matrice de corrélation et l'ACP.
%% 2.1 Matrice de correlation

correlation_matrix([X Y]);

% On observe des correlations suivantes:
% entre Y et (empvarrate, euribor3m, pdays, poutcome2, poutcome3, nremployed)
% sans surprise la probabilte de contracter un depot a long terme est
% fortement influencee par le nombre de jours passes apres que le client aie
% ete contacte.
%influence par le taux d'emploi
%influence par l'euribor
%influence par le nombre de commerciaux a la propection 


%% 2.2 ACP : essayons de reduire la dimension pour mieux visualiser notre probleme
[valpropres, U, moy]=mypca(X);
bar(valpropres/sum(valpropres));
title('Valeurs propres')
% Dimension des datas projetees
d=2;
P=U(:,1:d);
C=projpca(X,moy,P);
figure
plot(C(Y==1,1),C(Y==1,2),'ro')
hold on
plot(C(Y==0,1),C(Y==0,2),'bo')
title('affichage sur les deux plus grands axes de l acp')
% On remarque que l'acp ne nous aide pas car la reduction en 2 dimmensions
% n'est pas representative pour les donnees de notre genre.
%% 2.3 Nombre d'occurence des Y
% On va compter les occurences de chaque classe pour pouvoir determiner par
% la suite les algorithmes que nous allons choisir pour classifier nos donnees.  une SVM avec penalisation différente en fonction des points.

label0=sum(Y==0)
label1=sum(Y==1)
rapport=label1/(label0+label1)*100
% On constate un desequilibre d'ordre 10 entre les deux classes, il faudrait
% penser e� faire une SVM avec penalisation differente en fonction des points.
%% 3. Classification

%% 3.1 Downsampling
% Nous avons beaucoup de donnees et nos machines personnelles risquent de
% ne pas supporter autant alors nous alons utiliser la fonction
% downsampling pour reduire le nombre de donnees.
Z = [X Y];
[n p]=size(X);
Z = mydownsampling(Z,10);
X_small=Z(:,1:end-1);
Y_small=Z(:,end);
%% 3.2 Preparation des donnees pour la classification
% On va decouper nos donnees en 3 :
% donnees d'apprentissage : 1/6
% donnees de validation : 1/6
% donnees de test : 1/3
ratio =2/3;
[xapp, yapp, xtest, ytest] = splitdata(X_small, Y_small, ratio);
ratio = 1/2;
[xapp, yapp, xval, yval] = splitdata(xapp, yapp, ratio);

% On va ensuite centrer et reduire nos donnees
meanx = mean(xapp);
stdx=std(xapp);
[xapp,xval,~,~] = normalizemeanstd(xapp,xval,meanx,stdx);
[~,xtest,~,~] = normalizemeanstd(xapp,xtest,meanx,stdx);

%% 3.3 Methode des K-ppV
% La premiere methode de classification que nous avons traite est le K plus
% proche voisins. 
% On a decide de prendre dans un premier temps un k au hasard et en suite
% de chercher le k le plus adapte a nos données.
k=3;
[ypred,MatDist]=kppv(xval, xapp, yapp, k, []);
err_k_hasard = mean(yval~=ypred);

% Recherche du meilleur k : 
vectK=floor(linspace(1,20,10)); %echelle lineaire des valeurs de k possibles
% evaluation d'un modele
err_k_meilleur=zeros(length(vectK),1); %prealocation du vecteur pour gagner en performance
for i=1:1:length(vectK)
    [ypred,MatDist]=kppv(xval, xapp, yapp, vectK(i), []);
    err_k_meilleur(i) = mean(yval~=ypred); %calcul de l'erreur de validation
end

[val,pos] = min(err_k_meilleur); %On trouve l'indice du vecteur pour lequel l'erreur et minimale
meilleur_K=(vectK(pos)); % On selectione le meilleur k possible grace a l'indice

% Apres avoir trouve le meilleur K sur nos donnees de validation nous
% allons maintenant calculer la valeur de l'erreur sur le jeu de test avec
% le k choisi.

k=meilleur_K;
[ypred,MatDist]=kppv(xtest, xapp, yapp, k, []);
err_test = mean(ytest~=ypred);

disp('Meilleur K  Erreur validation   Erreur test')
disp([k val  err_test])

% On remarque alors une erreur de test assez petite.
%% 3.4 SVM 
%% 3.4.1 Normal
% Malgre la difference d'effectif des deux classes nous avons voulu quand meme
% illustrer le resultat d'un SVM Normal appliqué a nos données.


yapp(yapp==0)=-1;
ytest(ytest==0)=-1;
yval(yval==0)=-1;

% Evaluation d'un modele
vectC=logspace(-2,2,9) %echelle logarithmique
precision=zeros(length(vectC),1);
for i=1:1:length(vectC)
    [wapp,b]=monsvmclass(xapp,yapp,vectC(i));
    ypred=monsvmval(xval,wapp,b);
    ind_bon=find(ypred.*yval>0);
    n_bon=length(ind_bon); %nombre de valeurs bien classifies
    precision(i)=n_bon/length(yval);
end

[val,pos] = max(precision)
meilleur_C=(vectC(pos))

% On remarque que notre SVM a une precision de ~90% ce qui nous fait pense a 
% notre ordre de grande de presque 10 entre les effectifs. 
%On va ainsi essayer d'appliquer un SVM Penalise. 
%% 3.4.2 Avec penalisation
vectC=logspace(-1,3,9) %echelle logarithmique
vectCC=ones(size(yapp));
precision=zeros(length(vectC));
for i=1:1:length(vectC)
    for j=1:1:length(vectC)
        vectCC(yapp==1)= vectC(i);
        vectCC(yapp==-1)= vectC(j);
        [wapp,b]=monsvmclass(xapp,yapp,vectCC);
        ypred=monsvmval(xval,wapp,b);
        %f(x) et y on le meme signe ?
        ind_bon=find(ypred.*yval>0);
        n_bon=length(ind_bon); %nombre de valeurs bien classifies
        precision(i,j)=n_bon/length(yval);
    end
end
precision
[val,pos_i] = max(max(precision'));
[val,pos_j] = max(max(precision));

% On trouve  toujours une erreur proche de 10 avec le meilleur C (0.0316) qui meme si dans la vrai vie est une bonne
% performance, sur nos donnees n'est pas representatif.

%% 3.5 Implementation de LDA

% On va essayer maintenant de regarder le comportament d'une LDA sur notre
% jeu de donnees.

yapp(yapp==-1)=0;
ytest(ytest==-1)=0;
yval(yval==-1)=0;

ind1=find(yapp==1);
ind2=find(yapp==0);
n=length(yapp);
n1=length(ind1);
n2=length(ind2);
%calcul des mu
mu1_hat=mean(xapp(ind1,:))';
mu2_hat=mean(xapp(ind2,:))';
%calcul des pi
pi1_hat=n1/n;
pi2_hat=n2/n;
%calcul de la matrice de cov
C1_hat=(xapp(ind1,:)-ones(n1,1)*mu1_hat')'*(xapp(ind1,:)-ones(n1,1)*mu1_hat')./n1;
C2_hat=(xapp(ind2,:)-ones(n2,1)*mu2_hat')'*(xapp(ind2,:)-ones(n2,1)*mu2_hat')./n2;
%C1=cov(X(ind1,:))
%C2=cov(X(ind2,:))
C=(n1*C1_hat+n2*C2_hat)./n;
lamda=0.001;

C=C+lamda*eye(size(C));
% Calcul des paramtres de la fonction de dcision f
w=C\(mu1_hat-mu2_hat);
w0=log(pi1_hat/pi2_hat)-w'*(mu1_hat+mu2_hat)/2;

f=xapp*w+w0;

% Calcul de y chapeau hypothetique
Y_hat(f>0)=1;
Y_hat(f<=0)=0;
errapp=mean(yapp~=Y_hat') %erreur de classification



f = xtest*w + w0;
% Calcul de y chapeau hypothetique
Y_chap(f>0)=1;
Y_chap(f<=0)=0;



% Calcul de l'erreur
errtest = mean(Y_chap' ~= ytest)

% On a trouve alors encore une erreur en test proche de 10 ce qui est
% normal car LDA est aussi un algorithme de classification des modeles
% lineaires.


%% 4. Conclusion

%Nous avons obtenus des resultats concluants a l'aide de methodes non
%lineaires de type KppV cependant comme nos classes sont tres desequilibre
%nous avons constate un manque d'efficacite des modeles lineaires (qui
%mettent tout dans un meme cluster) il serait a l'avenir interressant
%d'augmenter le nombre de donnees ou bien essayer des techniques de SVM non
%lineaires.



