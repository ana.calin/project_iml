function  Ct = projpca(X , moy, P)
[n p]=size(X);
Xc=X-ones(n,1)*moy;
Ct=Xc*P;
end