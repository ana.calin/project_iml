function [output] = disjonctif2(input)
m=unique(input);
L=length(m);
[n,p]=size(input);
output=zeros(n,L);
% for k=1:1:L
%     for i=1:1:n
%         output(i,k)=strcmp(cellstr(input(i)),cellstr(m(k)));
%     end
% end
for k=1:1:L
    output(:,k)=strcmp(cellstr(input(:)),cellstr(m(k)));
end
end

