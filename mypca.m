function [valprop, U, moy] = mypca(X)
[n p]=size(X); 
moy=mean(X);
Xc=X-ones(n,1)*moy;
Mcov=(X'*X)/n;
[U, D]=eig(Mcov);
[valprop, index]= sort(diag(D), 'descend');
U=U(:,index);
end
