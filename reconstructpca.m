function Xhat= reconstructpca(C,P,moy)
  [n,p]=size(C);
  Xhat= C*P' + ones(n,1)*moy;
end