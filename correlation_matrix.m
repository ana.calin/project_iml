function [] = correlation_matrix(X)
%ind=find(ismember(CrimeSolved,'No'));
%X(ind,:)=[];
[n,p]=size(X);
Xn=(X-ones(n,1)*mean(X))./(ones(n,1)*std(X,1));
Mn=Xn'*Xn/n;
figure(1)
clf
imagesc(Mn); 
colorbar;
colormap(jet)
set(gca,'XTick',1:p)
set(gca,'YTick',1:p)
set(gca,'xticklabel',{'age', 'campaign', 'consconfidx', 'conspriceidx', 'default_1', 'default_2', 'default_3', 'education_1', 'education_2', 'education_3', 'education_4', 'education_5', 'education_6', 'education_7' ,'empvarrate', 'euribor3m','housing_1','housing_2','job_1','job_2','job_3','job_4','job_5','job_6','job_7','job_8','job_9','job_10','job_11', 'loan_1','loan_2', 'marital_1', 'marital_2', 'marital_3', 'nremployed', 'pdays','poutcome_1','poutcome_2','poutcome_3','Y'})
set(gca,'yticklabel',{'age', 'campaign', 'consconfidx', 'conspriceidx', 'default_1', 'default_2', 'default_3', 'education_1', 'education_2', 'education_3', 'education_4', 'education_5', 'education_6', 'education_7' ,'empvarrate', 'euribor3m','housing_1','housing_2','job_1','job_2','job_3','job_4','job_5','job_6','job_7','job_8','job_9','job_10','job_11', 'loan_1','loan_2', 'marital_1', 'marital_2', 'marital_3', 'nremployed', 'pdays','poutcome_1','poutcome_2','poutcome_3','Y'})
set(gca,'XTickLabelRotation',90)
title('Matrice des correlations')
end

