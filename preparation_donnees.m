function [X,Y] =preparation_donnees()
%% Import data from text file.

% Auto-generated by MATLAB on 2019/01/07 14:29:01

% Initialize variables.
filename = 'bank-additional-full.csv';
startRow = 2;
delimiter=';';
% Format for each line of text:
%   column1: double (%f)
%	column2: categorical (%C)
%   column3: categorical (%C)
%	column4: categorical (%C)
%   column5: categorical (%C)
%	column6: categorical (%C)
%   column7: categorical (%C)
%	column8: categorical (%C)
%   column9: categorical (%C)
%	column10: categorical (%C)
%   column11: double (%f)
%	column12: double (%f)
%   column13: double (%f)
%	column14: double (%f)
%   column15: categorical (%C)
%	column16: double (%f)
%   column17: double (%f)
%	column18: double (%f)
%   column19: double (%f)
%	column20: double (%f)
%   column21: categorical (%C)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%f%C%C%C%C%C%C%C%C%C%f%f%f%f%C%f%f%f%f%f%C%[^\n\r]';

% Open the text file.
fileID = fopen(filename,'r');

% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'EmptyValue', NaN, 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

% Close the text file.
fclose(fileID);

% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

% Allocate imported array to column variable names
age = dataArray{:, 1};
job = dataArray{:, 2};
marital = dataArray{:, 3};
education = dataArray{:, 4};
default = dataArray{:, 5};
housing = dataArray{:, 6};
loan = dataArray{:, 7};
contact = dataArray{:, 8};
month1 = dataArray{:, 9};
day_of_week = dataArray{:, 10};
duration = dataArray{:, 11};
campaign = dataArray{:, 12};
pdays = dataArray{:, 13};
previous = dataArray{:, 14};
poutcome = dataArray{:, 15};
empvarrate = dataArray{:, 16};
conspriceidx = dataArray{:, 17};
consconfidx = dataArray{:, 18};
euribor3m = dataArray{:, 19};
nremployed = dataArray{:, 20};
y = dataArray{:, 21};


% Clear temporary variables
clearvars filename delimiter startRow formatSpec fileID dataArray ans;
%% Codage Disjonctif y
ind_yes = find(y=='yes');
Y = zeros(length(y),1);
Y(ind_yes)=1;
%% Procedure suppression NAN value
X=table(age,campaign,consconfidx,conspriceidx,default,duration,education,empvarrate,euribor3m,housing,job,loan,marital,nremployed,pdays,poutcome,previous,Y);
%% Suppression des "unknown" de job et education
idx=any(strcmp(cellstr([job, education, housing, loan, marital]),'unknown'),2);
X(idx,:) = [];
Y(idx) = [];
%% Codage Disjonctif des autres variables
age=X.age;
campaign=X.campaign;
consconfidx=X.consconfidx;
conspriceidx=X.conspriceidx;
duration=X.duration;
empvarrate=X.empvarrate;
euribor3m=X.euribor3m;
nremployed=X.nremployed;
previous=X.previous;
pdays=X.pdays;
housing=disjonctif2(X.housing);
poutcome=disjonctif2(X.poutcome);
job=disjonctif2(X.job);
loan=disjonctif2(X.loan);
marital=disjonctif2(X.marital);
education=disjonctif2(X.education);
default=disjonctif2(X.default);


%%
% On passe en matriciel
X=[age campaign consconfidx conspriceidx default education empvarrate euribor3m housing job loan marital nremployed pdays poutcome];
end