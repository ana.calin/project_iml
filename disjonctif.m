function [output] = disjonctif(input)
    n=length(input);
    m=unique(input);
    output=[zeros(n,1)];
    for i=1:length(m)
        output(strcmp(cellstr(input),cellstr(m(i))))=i-1;
    end
end